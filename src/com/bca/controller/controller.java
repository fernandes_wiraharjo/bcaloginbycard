package com.bca.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.LogAdapter;
import adapter.LoginAdapter;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);
    static String apiName = "BCALoginByCard";
    static String apiMethod = "POST";

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIResult GetLoginCard(@RequestBody model.mdlLoginCard param) {
	long startTime = System.currentTimeMillis();
	String systemFunction = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlCardDetails cardDetails = new model.mdlCardDetails();

	model.mdlAPIResult mdlGetLoginCardResult = new model.mdlAPIResult();
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = param.WSID;
	mdlLog.SerialNumber = param.SerialNumber;
	mdlLog.ApiFunction = "loginByCard";
	mdlLog.SystemFunction = systemFunction;
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";

	List<model.mdlAccount> listAccountPIN, listAccount, listAccountFiltered = new ArrayList<model.mdlAccount>();

	Gson gson = new Gson();
	String jsonIn = gson.toJson(param);
	try {
	    if (param.CardNumber == null || param.CardNumber.equals("") || param.PINBlock == null || param.PINBlock.equals("")) {
		mdlErrorSchema.ErrorCode = "01";
		mdlMessage.Indonesian = "Nomor kartu atau PIN tidak valid";
		mdlMessage.English = mdlLog.ErrorMessage = "Card Number or PIN is not valid";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    }

	    // login by card
	    // call function to call BCA API to verify account using card number
	    // and PIN
	    model.mdlAPIResult mdlAPIResult = LoginAdapter.verifyPIN(param);

	    if (mdlAPIResult == null || mdlAPIResult.ErrorSchema == null) {
		mdlErrorSchema.ErrorCode = "02";
		mdlMessage.Indonesian = "Gagal memanggil service";
		mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlErrorSchema.ErrorCode = mdlAPIResult.ErrorSchema.ErrorCode;
		mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
		mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    }

	    LinkedTreeMap<String, JsonArray> outputSchemaMap = (LinkedTreeMap<String, JsonArray>) mdlAPIResult.OutputSchema;
	    JsonObject mObj = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
	    Type collectionType = new TypeToken<model.mdlCardDetails>() {
	    }.getType();
	    cardDetails = gson.fromJson(mObj.get("CardDetails").toString(), collectionType);

	    // CHECK IF RETURN CAN BE NOT NULL!!!
	    if (cardDetails == null) {
		mdlErrorSchema.ErrorCode = "04";
		mdlMessage.Indonesian = "Detail kartu kosong";
		mdlMessage.English = mdlLog.ErrorMessage = "Empty card details";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    }

	    collectionType = new TypeToken<List<model.mdlAccount>>() {
	    }.getType();
	    cardDetails.AccountDetails = gson.fromJson(mObj.get("AccountList").toString(), collectionType);
	    // cardDetails.ResultB24 = gson.fromJson(
	    // mObj.get("ResultB24").toString() , String.class);
	    cardDetails.ResultB24 = "";

	    // list of mdlAccount from mdlCardDetails
	    listAccountPIN = cardDetails.AccountDetails;

	    List<String> accountNumber = new ArrayList<String>();
	    // to contain account number values to be used in get account details function
	    // get account number from acoount list and put account number value to a list of string
	    for (model.mdlAccount account : listAccountPIN) {
		accountNumber.add(account.AccountNumber);
	    }
	    // set list of acccount number string into single string with comma
	    // delimiter ex: "123123,456456,789789"
	    StringJoiner joiner = new StringJoiner(",", "", "");
	    accountNumber.forEach(joiner::add);

	    // encode comma value in url parameter to utf8
	    // String accountNumberList = URLEncoder.encode(joiner.toString(),
	    // "UTF-8");
	    String accountNumberList = joiner.toString();

	    // call BCA API get account details by card number
	    mdlAPIResult = new model.mdlAPIResult();
	    mdlAPIResult = LoginAdapter.getAccountDetailsByCard(accountNumberList, param.SerialNumber, param.WSID);

	    if (mdlAPIResult.ErrorSchema == null) {
		mdlErrorSchema.ErrorCode = "09";
		mdlMessage.Indonesian = "Gagal mendapatkan data akun";
		mdlMessage.English = mdlLog.ErrorMessage = "Get account data failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlErrorSchema.ErrorCode = mdlAPIResult.ErrorSchema.ErrorCode;
		mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
		mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    }

	    outputSchemaMap = new LinkedTreeMap<String, JsonArray>();
	    mObj = new JsonObject();

	    outputSchemaMap = (LinkedTreeMap<String, JsonArray>) mdlAPIResult.OutputSchema;
	    mObj = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
	    collectionType = new TypeToken<List<model.mdlAccount>>() {
	    }.getType();
	    listAccount = gson.fromJson(mObj.get("AccountList").toString(), collectionType);

	    // CHECK IF RETURN CAN BE NOT NULL!!!
	    if (listAccount.size() == 0) {
		mdlErrorSchema.ErrorCode = "06";
		mdlMessage.Indonesian = "Jenis rekening Anda tidak memiliki buku tabungan.";
		mdlMessage.English = mdlLog.ErrorMessage = "Your Account Type Doesn't Have Book.";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    }

	    String isJointAccount = "false";
	    // if success get list of account, only show account that is not a
	    // joint account
	    for (model.mdlAccount account : listAccount) {
		// reuse condition to filter account type for tahapan, tahapan gold and tabunganku
		// if (account.AccountTypeDescription.equalsIgnoreCase("Tahapan") || account.AccountTypeDescription.equalsIgnoreCase("Tahapan
		// Gold")
		// || account.AccountTypeDescription.equalsIgnoreCase("TabunganKu")) {
		//
		// }
		for (model.mdlAccount accountPIN : listAccountPIN) {
		    // get the shortest length account number length from verpin and get account list account number
		    int accountLength = accountPIN.AccountNumber.length() <= account.AccountNumber.length() ? accountPIN.AccountNumber.length() : account.AccountNumber.length();
		    String accountVerPin = accountPIN.AccountNumber.substring(accountPIN.AccountNumber.length() - accountLength);
		    String accountFromList = account.AccountNumber.substring(account.AccountNumber.length() - accountLength);
		    if (accountVerPin.equals(accountFromList)) {
			account.AccountOwnership = accountPIN.AccountOwnership;
			break;
		    }
		}

		Integer customerCounter = account.CustomerDetails.size();
		if (customerCounter == 1) {
		    listAccountFiltered.add(account);
		} else {
		    isJointAccount = "false";
		    List<model.mdlCustomer> listCustomer = account.CustomerDetails;
		    for (model.mdlCustomer customer : listCustomer) {
			if (customer.OwnerCode.equalsIgnoreCase("902") && (customer.RelationType.equalsIgnoreCase("1")
				|| customer.RelationType.equalsIgnoreCase("2") || customer.RelationType.equalsIgnoreCase("3"))) {
			    isJointAccount = "true";
			}
		    }
		    if (param.JointAccount.equalsIgnoreCase("true")
			    || (param.JointAccount.equalsIgnoreCase("false") && isJointAccount.contentEquals("false"))) {
			listAccountFiltered.add(account);
		    }
		}

	    }

	    // error code jika nomor rekening adalah joint account
	    if ((listAccountFiltered.size() == 0) && isJointAccount.equalsIgnoreCase("true")) {
		mdlErrorSchema.ErrorCode = "10";
		mdlMessage.Indonesian = "Transaksi tidak dapat dilanjutkan";
		mdlMessage.English = mdlLog.ErrorMessage = "Transaction cannot be continued";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    } else if (listAccountFiltered.size() == 0) {
		mdlErrorSchema.ErrorCode = "07";
		mdlMessage.Indonesian = "Jenis rekening Anda tidak memiliki buku tabungan.";
		mdlMessage.English = mdlLog.ErrorMessage = "Your Account type doesn't have book.";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		String jsonOut = gson.toJson(mdlGetLoginCardResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "", jsonIn, jsonOut));
		return mdlGetLoginCardResult;
	    } else {
		mdlErrorSchema.ErrorCode = "00";
		mdlMessage.Indonesian = "Berhasil";
		mdlMessage.English = mdlLog.LogStatus = "Success";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
		mdlGetLoginCardResult.OutputSchema = listAccountFiltered;
	    }
	    String jsonOut = gson.toJson(mdlGetLoginCardResult);
	    logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, apiName, apiMethod, "", jsonIn, jsonOut));
	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "08";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
	    mdlLog.ErrorMessage = ex.toString();
	    String jsonOut = gson.toJson(mdlGetLoginCardResult);
	    logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, apiName, apiMethod, "", jsonIn, jsonOut, ex.toString()), ex);
	}

	LogAdapter.InsertLog(mdlLog);
	return mdlGetLoginCardResult;
    }

}

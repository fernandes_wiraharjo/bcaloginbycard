package model;

import java.util.List;

public class mdlCardDetails {
	public String CardNumber;
	public String LanguageID;
	public String CurrentDate;
	public String OpenDate;
	public String ProductionDate;
	public String CustomerName;
	public List<String> CustomerAddress; //array of string
	public String BranchCode;
	public String ProductionCardFlag;
	public String ChargeFlag;
	public String Outstanding;
	public String SendFlag;
	public String CardCompanyBrand;
	public String AccountCounters;
	public List<model.mdlAccount> AccountDetails; // object from model mdlAccount
	public String ResultB24;
}

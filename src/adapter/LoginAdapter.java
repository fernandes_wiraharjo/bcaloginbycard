package adapter;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import model.Globals;

public class LoginAdapter {
    final static Logger logger = LogManager.getLogger(LoginAdapter.class);
    static Client client = Client.create();

    public static model.mdlAPIResult verifyPIN(model.mdlLoginCard mdlLoginCard) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "loginByCard";
	mdlLog.SystemFunction = "verifyPIN";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = mdlLoginCard.SerialNumber;
	mdlLog.WSID = mdlLoginCard.WSID;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	String jsonIn, jsonOut = "";
	String cardNumber = mdlLoginCard.CardNumber;
	Gson gson = new Gson();

	String urlAPI = "/debit-cards/pin-verification/card-numbers/" + cardNumber;

	jsonIn = gson.toJson(mdlLoginCard);
	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    Globals.gCommand = urlAPI;

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);

	    jsonIn.replaceAll("\\s+", ""); // canonicalize JSON (remove all
					   // whitespace like \r, \n, \t and
					   // space)

	    ClientResponse response = webResource.type("application/json").header("ClientID", decryptedClientID).post(ClientResponse.class, jsonIn);
	    jsonOut = response.getEntity(String.class);

	    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlAPI, "POST", "function: " + functionName, jsonIn, jsonOut));

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, urlAPI, "POST", "function: " + functionName, jsonIn, jsonOut, ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }

    public static model.mdlAPIResult getAccountDetailsByCard(String accountNumberList, String SerialNumber, String WSID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "loginByCard";
	mdlLog.SystemFunction = "getAccountDetailsByCard";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";
	String jsonOut = "";

	String urlAPI = "/deposit-accounts/account-number-lists/" + accountNumberList;

	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);

	    ClientResponse response = webResource.type("application/json").header("ClientID", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);

	    logger.info(LogAdapter.logToLog4j(true, startTime, 200, urlAPI, "POST", "function : " + functionName + ", accountNumberList:"
		    + accountNumberList + ", SerialNumber : " + SerialNumber + ", WSID : " + WSID, "", jsonOut));

	    Gson gson = new Gson();
	    mdlAPIResult = gson.fromJson(jsonOut, mdlAPIResult.getClass());
	    if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, urlAPI, "POST", "function : " + functionName + ", accountNumberList:"
		    + accountNumberList + ", SerialNumber : " + SerialNumber + ", WSID : " + WSID, "", jsonOut, ex.toString()), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }
}
